(function ($) {

    $(".toggle-menu-btn").on("click", function (e) {
        e.preventDefault();
        if (!$(this).hasClass("active")) {
            $(this).addClass("active");
            $(".nav-bar").addClass("active");
        } else {
            $(this).removeClass("active");
            $(".nav-bar").removeClass("active");
        }
    });

    $(".search-btn").on("click", function(e){
        e.preventDefault();
        $('.search-box').addClass('active');
        $('.search-box input[type=text]').focus();
    });

    $(".search-box .close").on("click", function(e){
        e.preventDefault();
        $('.search-box').removeClass('active');
    });

    //Show bio
    $('.bio-section .bio-toggle').on('click', function(e){
        e.preventDefault();
        $('.bio-section .wrapper').slideToggle(200);
        $(this).find('span').toggleClass('hidden');
    })

    $('.family-tree__card-option').on('click', function(e){
        e.preventDefault();
        var rootID = '#root-' + $(this).data('root-id');
        //$('.family-tree-hidden').css('display', 'none');
        $(rootID).slideDown(1000, function(){
            $('html, body').animate({
                scrollTop: $(rootID).offset().top
            }, 1000);
        });
    });

})(jQuery);